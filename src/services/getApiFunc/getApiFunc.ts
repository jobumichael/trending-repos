export const getApiFunc = async (url: string, options?: any) =>
  fetch(url, options);
