import { getSearchApiUrl } from "./getSearchApiUrl";

describe("getApiUrl", () => {
  it("should return the correct url", () => {
    const url = getSearchApiUrl("all");
    const lastWeekDate = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000)
      .toISOString()
      .slice(0, 10);
    expect(url).toBe(
      `https://api.github.com/search/repositories?q=created:>${lastWeekDate}&order=desc&sort=stars`
    );
  });

  it("should return the correct url with language", () => {
    const url = getSearchApiUrl("all", { language: "javascript" });
    const lastWeekDate = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000)
      .toISOString()
      .slice(0, 10);

    expect(url).toBe(
      `https://api.github.com/search/repositories?q=created:>${lastWeekDate} language:javascript&order=desc&sort=stars`
    );
  });

  it("should return empty string if repoType is starred and repoNames is empty", () => {
    const url = getSearchApiUrl("starred");

    expect(url).toBe("");
  });

  it("should return the correct url if repoType is starred and repoNames is not empty", () => {
    const url = getSearchApiUrl("starred", { repoNames: ["facebook/react"] });

    expect(url).toBe(
      `https://api.github.com/search/repositories?q=repo:facebook/react &order=desc&sort=stars`
    );
  });
});
