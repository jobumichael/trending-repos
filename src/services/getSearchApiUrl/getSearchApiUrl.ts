import { BASE_GITHUB_API_URL } from "config";
import { TRepoType } from "context/AppContext";

type GetApiUrlOptions = {
  language?: string;
  date?: string;
  order?: string;
  sort?: string;
  repoNames?: string[];
};

export const getSearchApiUrl = (
  repoType: TRepoType,
  options?: GetApiUrlOptions
) => {
  const lastWeekDate = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000)
    .toISOString()
    .slice(0, 10);
  const {
    language,
    date = lastWeekDate,
    order = "desc",
    sort = "stars",
    repoNames = [],
  } = options || {};

  if (repoType === "starred" && repoNames.length === 0) {
    return "";
  }

  let query = "";

  if (repoType === "starred" && repoNames.length > 0) {
    query = repoNames.reduce((acc, repoName) => {
      acc += `repo:${repoName} `;
      return acc;
    }, query);
  } else {
    query = `created:>${date}`;
  }

  if (language) {
    query = `${query} language:${language}`;
  }
  query = `${query}&order=${order}&sort=${sort}`;

  return `${BASE_GITHUB_API_URL}/search/repositories?q=${query}`;
};
