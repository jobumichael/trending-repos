import React from "react";
import * as S from "./Button.styles";

interface IProps {
  children: React.ReactNode;
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
}

const Button: React.FC<IProps> = ({ children, onClick }) => {
  return <S.Button onClick={onClick}>{children}</S.Button>;
};

export default Button;
