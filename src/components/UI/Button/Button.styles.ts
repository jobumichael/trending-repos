import styled from "styled-components";

export const Button = styled.button`
  background: transparent;
  cursor: pointer;
  border: 1px solid #e1e4e8;
  border-radius: 0.25rem;
  padding: 0.25rem 0.25rem;
  font-size: 0.85rem;
  font-weight: 800;
  color: #586069;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 6rem;
  outline: none;
  padding: 5px 16px;
  color: #24292f;
  border: 1px solid #d0d7de;
`;
