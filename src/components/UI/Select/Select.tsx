import React from "react";
import * as S from "./Select.styles";

interface IProps {
  handleOnChange: (selectedLanguage: string) => void;
  options: string[];
}

const Select: React.FC<IProps> = ({ handleOnChange, options }) => {
  const handleSelectChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    handleOnChange(e.target.value);
  };

  return (
    <S.SelectWrapper>
      <label htmlFor="language">Select language:</label>
      <S.Select
        data-testid="language"
        id="language"
        onChange={handleSelectChange}
      >
        <option value="">All</option>
        {options.map((language) => (
          <option key={language} value={language}>
            {language}
          </option>
        ))}
      </S.Select>
    </S.SelectWrapper>
  );
};

export default Select;
