import "@testing-library/jest-dom/extend-expect";
import { cleanup, render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";
import Select from "./Select";

afterEach(cleanup);

const options = ["JavaScript", "TypeScript", "Python", "Java", "C++"];

describe("<Select/>", () => {
  it("should render Select", () => {
    const { getAllByRole } = render(
      <Select options={options} handleOnChange={jest.fn()} />
    );

    expect(getAllByRole("option").length).toBe(6);
  });

  it("should change value", () => {
    const { getByTestId } = render(
      <Select options={options} handleOnChange={jest.fn()} />
    );
    userEvent.selectOptions(getByTestId("language"), "JavaScript");

    expect(getByTestId("language").value).toBe("JavaScript");
  });
});
