import styled from "styled-components";

export const Select = styled.select`
  background: transparent;
  border: none;
  cursor: pointer;
  border: 1px solid #e1e4e8;
  border-radius: 0.25rem;
  padding: 0.25rem 0.5rem;
  font-size: 0.85rem;
  font-weight: 800;
  color: #586069;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-left: 1rem;
  width: 10rem;
  height: 2rem;
  outline: none;
`;

export const SelectWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  @media (max-width: 768px) {
    width: 100%;
  }

  label {
    font-size: 0.85rem;
    font-weight: 500;
    color: #586069;
  }
`;
