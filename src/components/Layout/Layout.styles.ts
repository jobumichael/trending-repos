import styled from "styled-components";

export const Main = styled.main`
  width: 900px;
  margin: 0 auto;
  padding: 0 1rem;
  min-height: calc(100vh - 160px);

  @media (max-width: 768px) {
    width: 98%;
    margin: 0 1%;
    padding: 0;
  }
`;
