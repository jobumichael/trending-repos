import React from "react";
import * as S from "./Footer.styles";

const Footer = () => {
  return (
    <S.Footer>
      <p>Created By Jobe</p>
    </S.Footer>
  );
};

export default Footer;
