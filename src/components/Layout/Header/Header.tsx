import React from "react";
import * as S from "./Header.styles";

interface IProps {
  children?: React.ReactNode;
}

const Header: React.FC<IProps> = ({ children }) => {
  return (
    <S.Header>
      <h1>Trending repositories</h1>
      <div>{children}</div>
    </S.Header>
  );
};

export default Header;
