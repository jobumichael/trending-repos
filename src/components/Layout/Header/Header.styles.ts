import styled from "styled-components";

export const Header = styled.header`
  height: 100px;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #f5f5f5;
  color: #777;
  font-size: 0.9rem;
  font-weight: 300;
  line-height: 1.5;
  letter-spacing: 0.05rem;
  text-transform: uppercase;
`;
