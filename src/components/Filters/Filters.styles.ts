import { TRepoType } from "context/AppContext";
import styled, { css } from "styled-components";

export const SelectedButtonStyles = css`
  background-color: #0969da;
  border-color: #0969da;
  color: #fff;
`;

export const ButtonsWrapper = styled.div<{ repoType: TRepoType }>`
  display: inline-flex;

  @media (max-width: 768px) {
    width: 100%;
    margin-bottom: 1rem;

    button {
      width: 50%;
    }
  }

  & button:first-child {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    border-right: none;
    ${({ repoType }) => repoType === "all" && SelectedButtonStyles}
  }

  & button:last-child {
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
    border-left: none;
    ${({ repoType }) => repoType === "starred" && SelectedButtonStyles}
  }
`;

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  margin-top: 30px;
  margin-bottom: 30px;
  flex-wrap: wrap;
`;
