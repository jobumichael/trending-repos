import Button from "components/UI/Button/Button";
import Select from "components/UI/Select/Select";
import { languages } from "config/languages";
import { TRepoType, useAppContext } from "context/AppContext";
import React from "react";
import * as S from "./Filters.styles";

interface IProps {
  resetData: () => void;
  setSelectedLanguage: (language: string) => void;
}

const Filters: React.FC<IProps> = ({ resetData, setSelectedLanguage }) => {
  const { repoType, updateRepoType } = useAppContext();

  const handleClick = (e: React.MouseEvent<HTMLButtonElement>) => {
    resetData();
    const repoType = e.currentTarget.innerText;
    updateRepoType(repoType.toLowerCase() as TRepoType);
  };

  const handleOnChange = (selectedLanguage: string) => {
    setSelectedLanguage(selectedLanguage);
    resetData();
  };

  return (
    <S.Wrapper>
      <S.ButtonsWrapper repoType={repoType}>
        <Button onClick={handleClick}>All</Button>
        <Button onClick={handleClick}>Starred</Button>
      </S.ButtonsWrapper>
      <Select
        handleOnChange={handleOnChange}
        options={Object.keys(languages)}
      />
    </S.Wrapper>
  );
};

export default Filters;
