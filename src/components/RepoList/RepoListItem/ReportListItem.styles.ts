import styled from "styled-components";

export const Wrapper = styled.div`
  padding: 1rem;
  display: -webkit-flex;
  display: flex;
  position: relative;
  border: 1px solid #e1e4e8;
  /* border: 1px solid #e1e4e8; */
  border-radius: 6px;
  box-shadow: 0.25rem 0.25rem 0.6rem rgba(0, 0, 0, 0.05),
    0 0.5rem 1.125rem rgba(75, 0, 0, 0.05);
  background-color: #fff;
  background-image: linear-gradient(-180deg, #fafbfc, #eff3f6 90%);
  flex-direction: column;
  margin: 0 auto;
  margin-bottom: 1rem;

  &:hover {
    background-color: #f6f8fa;
    background-image: none;
    border-color: #e1e4e8;
    box-shadow: 0.25rem 0.25rem 0.6rem rgba(0, 0, 0, 0.05);
  }
`;

export const ListHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 1rem;
  a {
    text-decoration: none;
    color: #0366d6;
    font-size: 1rem;
    font-weight: 600;
  }
`;

export const Description = styled.div`
  font-size: 0.9rem;
  color: #586069;
`;

export const LanguageWrapper = styled.span`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-right: 1rem;
  padding: 0.25rem 0.5rem;
  border-radius: 0.25rem;
  font-size: 0.75rem;
  font-weight: 600;
`;

export const LanguageCircle = styled.span<{ languageColor: string }>`
  display: inline-block;
  width: 0.75rem;
  height: 0.75rem;
  margin-right: 0.5rem;
  border-radius: 50%;
  background-color: ${(props) => props.languageColor};
`;

export const ListFooter = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 1rem;

  button {
    background: transparent;
    border: none;
    cursor: pointer;
    border: 1px solid #e1e4e8;
    border-radius: 0.25rem;
    padding: 0.25rem 0.5rem;
    font-size: 0.75rem;
    font-weight: 600;
    color: #586069;
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 120px;

    span {
      margin-left: 0.5rem;
    }

    &:hover {
      background-color: #f6f8fa;
      background-image: none;
      border-color: #e1e4e8;
      box-shadow: 0.25rem 0.25rem 0.6rem rgba(0, 0, 0, 0.05);
    }
  }
`;

export const FooterItem = styled.div`
  display: flex;
  align-items: center;
  font-size: 0.75rem;
  color: #586069;
  margin-right: 1rem;

  span {
    display: flex;
    align-items: center;
  }
`;

export const AvatarWrapper = styled.div`
  margin-right: 0.5rem;
  border-radius: 50%;
  background-color: rgba(207, 211, 214, 0.5);
  width: 2rem;
  height: 2rem;
  border: 2px solid rgba(207, 211, 214, 0.5);

  img {
    border-radius: 50%;
    max-width: 100%;
    height: auto;
  }
`;
