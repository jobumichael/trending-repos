import Star from "components/Icons/Star";
import { languages } from "config/languages";
import { useAppContext } from "context/AppContext";
import { IRepository } from "interface/repo.interface";
import React from "react";
import { numFormatter } from "utils/kFormatter";
import * as S from "./ReportListItem.styles";

interface IProps {
  repository: IRepository;
  resetData: () => void;
}

const RepoListItem: React.FC<IProps> = ({ repository, resetData }) => {
  const {
    full_name,
    name,
    description,
    html_url,
    stargazers_count,
    language,
    owner: { avatar_url, login },
  } = repository;

  const { repoNames, repoType, updateRepoNames, removeRepoNames } =
    useAppContext();

  const handleStarClick = () => {
    const isRepoInList = repoNames.includes(full_name);

    if (isRepoInList) {
      removeRepoNames(full_name);
      if (repoType === "starred") {
        resetData();
      }
    } else {
      updateRepoNames(full_name);
    }
  };
  const IsStared = repoNames.includes(full_name);

  return (
    <S.Wrapper>
      <S.ListHeader>
        <a href={html_url} target="_blank" rel="noreferrer noopener">
          {full_name}
        </a>
        <S.LanguageWrapper>
          <S.LanguageCircle languageColor={languages[language]} />
          <span>{language}</span>
        </S.LanguageWrapper>
      </S.ListHeader>
      <S.Description>{description}</S.Description>
      <S.ListFooter>
        <S.FooterItem>
          <S.AvatarWrapper>
            <img src={avatar_url} loading="lazy" alt={`${name} avatar`} />
          </S.AvatarWrapper>
          <span>{login}</span>
        </S.FooterItem>
        <S.FooterItem>
          <button data-testid="btnStar" onClick={handleStarClick}>
            {IsStared ? <Star fill="#eac54f" /> : <Star />}
            <span>{IsStared ? "starred" : "star"}</span>
            <span>{numFormatter(stargazers_count + +!!IsStared)}</span>
          </button>
        </S.FooterItem>
      </S.ListFooter>
    </S.Wrapper>
  );
};

export default RepoListItem;
