import "@testing-library/jest-dom/extend-expect";
import { cleanup, render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";
import RepoListItem from "./RepoListItem";

afterEach(cleanup);

jest.mock("../../../context/AppContext", () => ({
  useAppContext: () => ({
    repoNames: ["test"],
    repoType: "all",
    updateRepoNames: jest.fn(),
    removeRepoNames: jest.fn(),
  }),
}));

const mockData = {
  full_name: "test",
  name: "name",
  description: "simple description",
  html_url: "",
  stargazers_count: 1,
  language: "JavaScript",
  owner: {
    avatar_url: "test",
    login: "test",
  },
};

describe("<RepoListItem/>", () => {
  it("should render RepoList item", () => {
    const { container } = render(
      <RepoListItem repository={mockData} resetData={jest.fn()} />
    );

    expect(
      container.getElementsByTagName("span")[0].innerHTML.includes("JavaScript")
    ).toBeTruthy();
    expect(container.getElementsByTagName("span").length).toBe(6);
  });

  it("should star the repo", () => {
    const { container, getByTestId } = render(
      <RepoListItem repository={mockData} resetData={jest.fn()} />
    );

    userEvent.click(getByTestId("btnStar"));

    expect(container.getElementsByTagName("span")[4].innerHTML).toBe("starred");
    expect(container.getElementsByTagName("span")[5].innerHTML).toBe("2");
  });
});
