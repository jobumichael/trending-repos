import { IRepository } from "interface/repo.interface";
import React from "react";
import RepoListItem from "./RepoListItem/RepoListItem";

interface IProps {
  data: Array<IRepository>;
  resetData: () => void;
}

const RepoList: React.FC<IProps> = ({ data, resetData }) => {
  if (!data) {
    return (
      <>
        <p>No data</p>
      </>
    );
  }

  return (
    <>
      <ul>
        {data.map((repo) => (
          <li key={repo.name}>
            <RepoListItem repository={repo} resetData={resetData} />
          </li>
        ))}
      </ul>
    </>
  );
};

export default RepoList;
