import "@testing-library/jest-dom/extend-expect";
import { cleanup, render } from "@testing-library/react";
import React from "react";
import RepoList from "./RepoList";

afterEach(cleanup);

const mockData = {
  full_name: "test",
  name: "name",
  description: "simple description",
  html_url: "",
  stargazers_count: 1,
  language: "JavaScript",
  owner: {
    avatar_url: "test",
    login: "test",
  },
};

describe("<RepoList/>", () => {
  it("should render RepoList", () => {
    const { container } = render(
      <RepoList data={[mockData]} resetData={jest.fn()} />
    );

    expect(
      container.getElementsByTagName("span")[0].innerHTML.includes("JavaScript")
    ).toBeTruthy();
    expect(container.getElementsByTagName("span").length).toBe(6);
  });
});
