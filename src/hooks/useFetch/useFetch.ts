import { useState } from "react";

export const useFetch = (apiFunc: any) => {
  const [data, setData] = useState(null);
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);

  const resetData = () => {
    setData(null);
    setError("");
    setLoading(false);
  };

  const request = async (...args: any) => {
    setLoading(true);
    try {
      const result = await apiFunc(...args);

      const data = await result.json();
      setData(data);
    } catch (error) {
      setError(error as any);
    } finally {
      setLoading(false);
    }
  };

  return {
    data,
    error,
    loading,
    request,
    resetData,
  };
};
