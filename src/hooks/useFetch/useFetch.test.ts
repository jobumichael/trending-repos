import { act, renderHook } from "@testing-library/react-hooks";
import { getApiFunc } from "../../services/getApiFunc/getApiFunc";
import { useFetch } from "./useFetch";

describe("useFetch", () => {
  it("should return success response", async () => {
    global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve({ test: 100 }),
      })
    ) as jest.Mock;
    const setUpHook = () => renderHook(() => useFetch(getApiFunc));
    const { result } = setUpHook();

    expect(result.current.data).toBeNull();

    await act(async () => {
      await result.current.request("/some-url");
    });

    expect(result.current.loading).toBeFalsy();
    expect(result.current.data).toStrictEqual({ test: 100 });
  });

  it("should return error response", async () => {
    global.fetch = jest.fn(() =>
      Promise.reject({
        json: () => Promise.reject({ test: 100 }),
      })
    ) as jest.Mock;

    const setUpHook = () => renderHook(() => useFetch(getApiFunc));
    const { result } = setUpHook();

    expect(result.current.data).toBeNull();

    await act(async () => {
      await result.current.request("/some-url");
    });

    expect(result.current.loading).toBeFalsy();
    expect(result.current.data).toStrictEqual(null);
  });

  it("should reset data", async () => {
    global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve({ test: 100 }),
      })
    ) as jest.Mock;

    const setUpHook = () => renderHook(() => useFetch(getApiFunc));
    const { result } = setUpHook();

    expect(result.current.data).toBeNull();

    await act(async () => {
      await result.current.request("/some-url");
    });

    expect(result.current.loading).toBeFalsy();
    expect(result.current.data).toStrictEqual({ test: 100 });

    act(() => {
      result.current.resetData();
    });

    expect(result.current.data).toBeNull();
  });
});
