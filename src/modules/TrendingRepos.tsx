/* eslint-disable react-hooks/exhaustive-deps */
import Filters from "components/Filters/Filters";
import RepoList from "components/RepoList/RepoList";
import { useAppContext } from "context/AppContext";
import { useFetch } from "hooks/useFetch/useFetch";
import React, { useEffect, useState } from "react";
import { getApiFunc } from "services/getApiFunc/getApiFunc";
import { getSearchApiUrl } from "services/getSearchApiUrl/getSearchApiUrl";

function TrendingRepos() {
  const { request, loading, error, data, resetData } = useFetch(
    getApiFunc
  ) as any;
  const { repoType, repoNames } = useAppContext();

  const [selectedLanguage, setSelectedLanguage] = useState("");

  useEffect(() => {
    if (!data) {
      const apiUrl = getSearchApiUrl(repoType, {
        repoNames,
        language: selectedLanguage,
      });
      if (apiUrl) {
        request(apiUrl);
      }
    }
  }, [repoType, data, selectedLanguage]);

  return (
    <>
      <Filters
        resetData={resetData}
        setSelectedLanguage={setSelectedLanguage}
      />
      {loading ? (
        "loading..."
      ) : (
        <RepoList data={data?.items} resetData={resetData} />
      )}
    </>
  );
}

export default TrendingRepos;
