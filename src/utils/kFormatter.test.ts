import { numFormatter } from "./kFormatter";

describe("numFormatter", () => {
  it("should return the number if it is less than 1000", () => {
    expect(numFormatter(999)).toBe(999);
  });

  it("should return the number in K format if it is greater than 1000", () => {
    expect(numFormatter(1000)).toBe("1K");
  });
});
