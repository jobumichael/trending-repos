import React, { createContext, FC, useContext, useState } from "react";

export type TRepoType = "all" | "starred";
interface IAppContext {
  repoNames: string[];
  repoType: TRepoType;
  updateRepoType: (type: TRepoType) => void;
  updateRepoNames: (repoName: string) => void;
  removeRepoNames: (repoName: string) => void;
}

const getStoredCart = () => {
  if (typeof window === "undefined") {
    return [] as string[];
  } else {
    const storedData = localStorage.getItem("repoNames");
    return storedData ? JSON.parse(storedData) : ([] as string[]);
  }
};

const storeData = (repoNames: string[]) => {
  if (typeof window !== "undefined") {
    localStorage.setItem("repoNames", JSON.stringify(repoNames));
  }
};

const AppContext = createContext<IAppContext>({
  repoNames: [],
  repoType: "all",
  updateRepoType: () => {},
  updateRepoNames: () => {},
  removeRepoNames: () => {},
});

export const AppProvider: FC = ({ children }) => {
  const [repoNames, setRepoNames] = useState<string[]>(getStoredCart() || []);
  const [repoType, setRepoType] = useState<TRepoType>("all");

  const updateRepoNames = (repoName: string) => {
    const updatedNames = [...repoNames, repoName];

    setRepoNames(updatedNames);
    storeData(updatedNames);
  };

  const removeRepoNames = (repoName: string) => {
    const updatedNames = repoNames.filter(function (name) {
      return name !== repoName;
    });

    setRepoNames(updatedNames);
    storeData(updatedNames);
  };

  const updateRepoType = (type: TRepoType) => {
    setRepoType(type);
  };

  return (
    <AppContext.Provider
      value={{
        repoNames,
        repoType,
        updateRepoType,
        updateRepoNames,
        removeRepoNames,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

export const useAppContext = () => {
  return useContext(AppContext);
};
