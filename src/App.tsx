/* eslint-disable react-hooks/exhaustive-deps */
import Layout from "components/Layout/Layout";
import { AppProvider } from "context/AppContext";
import TrendingRepos from "modules/TrendingRepos";
import React from "react";

function App() {
  return (
    <AppProvider>
      <Layout>
        <TrendingRepos />
      </Layout>
    </AppProvider>
  );
}

export default App;
